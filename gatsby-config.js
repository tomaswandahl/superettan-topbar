module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
      {
         resolve: `gatsby-plugin-s3`,
              options: {
                  bucketName:  "topbar-superettan",
                  region: "eu-north-1",
              },
          },

      `gatsby-plugin-react-helmet`,
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `images`,
          path: `${__dirname}/src/img`,
        },
      },

      'gatsby-source-everysport',
      {
          resolve: "gatsby-source-everysport",
          options: {
              apikey: "4f39ee62012cdf3d13f3e9060c44248d",
          },
      },


    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // 'gatsby-plugin-offline',
  ],

}
