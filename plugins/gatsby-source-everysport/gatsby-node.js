const fetch = require("node-fetch")
const queryString = require("query-string")

exports.sourceNodes = (
  { actions, createNodeId, createContentDigest },
  configOptions
) => {
    const { createNode } = actions

    // Gatsby adds a configOption that's not needed for this plugin, delete it
    delete configOptions.plugins

    const processStandings = standings => {
      const nodeId = createNodeId(`allsvenskan-tabell`)
      const nodeContent = JSON.stringify(standings)
      const nodeData = Object.assign({}, standings, {
        id: nodeId,
        parent: null,
        children: [],
        internal: {
          type: `Everysport`,
          content: nodeContent,
          contentDigest: createContentDigest(standings),
        },
      })

      return nodeData
    }


    // Convert the options object into a query string
    const apiOptions = queryString.stringify(configOptions)
    // Join apiOptions with the Pixabay API URL
    const apiUrl = `http://api.everysport.com/v1/leagues/100878/standings/?${apiOptions}`
    //const apiUrl = `http://api.everysport.com/v1/leagues/58878/standings`
    //http://api.everysport.com/v1/leagues/58878/standings/?${apiOptions}
    // Gatsby expects sourceNodes to return a promise
    return (
      // Fetch a response from the apiUrl
      fetch(apiUrl)
        // Parse the response as JSON
        .then(response => response.json())
        // Process the JSON data into a node
        .then(data => {
          // For each query result (or 'hit')
          const nodeData = processStandings(data)
          // Use Gatsby's createNode helper to create a node from the node data
          createNode(nodeData)
        })
    )
  }
