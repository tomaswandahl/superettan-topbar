import React, { Component } from "react"
import { graphql } from "gatsby"
import Table from "../components/table"
//import TableEntry from "../components/tableEntry"
import sef from "../img/sef.png"
import superettan from "../img/superettan.png"
import select from "../img/Select_white.png"
import opg from "../img/opg.png"
//import strata from "../fonts/Strata/desktop/Strata.otf"
import $ from 'jquery'

/* eslint-disable */

export const GatsbyQuery = graphql`
{
  allEverysport {
    edges {
      node {
        groups {
          standings {
            team {
              name
              shortName
            }
            position
            stats {
              name
              value
            }
          }
        }
      }
    }
  }
}
`

class ClientFetchingExample extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const {
          allEverysport: { edges },

        } = this.props.data;

        const menu = {
              "menu": {
                "games": {
                  "text": "Spelprogram",
                  "href": "https://www.allsvenskan.se/matcher"
                },
                "table": {
                  "text": "Tabell"
                },
                "scores": {
                  "text": "Skytteliga",
                  "href": "http://"
                },
              }
          };

          const ads = {
                "company": {
                    "svenskaspel": {
                        "name": "Svenska Spel",
                        "url": "http://",
                        "image": "http://efd.se/wp-content/themes/efd/images/svenskaspel2019.png",
                        "percentage": 25
                    },
                    "dhl": {
                        "name": "DHL",
                        "url": "http://",
                        "image": "https://old.allsvenskan.se/app/uploads/2018/08/DHL.png",
                        "percentage": 15
                    },
                    "onepartnergroup": {
                        "name": "OnePartnerGroup",
                        "url": "http://",
                        "image": "https://uc9006d57fe3de6d72f01d665eb6.previews.dropboxusercontent.com/p/thumb/AAab-HPjUIGg-5RuC96iJoR1dRuzVJoxmlXiACHk0L_pA0iyEFPF1aV_7LsoNhmKP_AiirD_CoKfcO3iLfwKn_7M3Nhr54WZt7gRizRz0rPC7U2ikWRf-Z7mXpfmEVW3livxhtRR0TFBx7LUblQXg_qHs7tgNzpCvzXJHzSeUaABQXifZMlIUJT5-dZZ_gKeBgtXUe5c73EBZ-jtnxMPiM_5imBw_XU8JgFb8sHUBA_oipoVipj1XbJfJMea_OywL8Gc83zEtk1v7Rcz2hJOns9WqZLw5-JnQ-_DuMF6OGm5q3SqwBKYjF7gyAW_djb3ShiyAekrSs0Sshp8xX7b8UtI2ycF2Pt3QskWFVazUEFYLt_LEBDK6HxH-wbFdcBFQ2euxQmlNZUdLKn4WOIKrBVH/p.png?size_mode=5",
                        "percentage": 15
                    },
                    "obos": {
                        "name": "OBOS",
                        "url": "http://",
                        "image": "http://efd.se/wp-content/themes/efd/images/obos_top_2019.png",
                        "percentage": 15
                    },
                    "vw": {
                        "name": "VW",
                        "url": "http://",
                        "image": "https://old.allsvenskan.se/app/uploads/2018/08/VW.png",
                        "percentage": 15
                    },
                    "select": {
                        "name": "Select",
                        "url": "http://",
                        "image": "",
                        "percentage": 15
                    }
                }
          };

          console.log({select});

          // Ad-handling
          var ad_val = (Math.floor(Math.random() * 100)) + 1;

          var i = 0;
          var lowerThreshold = 0;
          var company = null;
          var display_ad = null;

          Object.keys(ads.company).forEach(function(key){
              company = ads.company[key];
              if((ad_val <= (lowerThreshold + company.percentage)) && (ad_val > lowerThreshold)) {
                  display_ad = i;
              } else {

              }
              lowerThreshold += company.percentage;
              i += 1;
          });



          switch(display_ad) {
                case 5:
                    $('.select').addClass('display');
                    break;
                case 2:
                    $('.opg').addClass('display');
                    break;
                default:
                    var displayKey = Object.keys(ads.company)[display_ad];
                    var displayImage = "<img class='"+displayKey+"' src='"+ads.company[displayKey].image+"' alt='' />";
                    $('.sponsors').html(displayImage);
                    break;
          }

          // Read the menu-json and populate the topbar-menu
          var table = $('.league-table').html();
          $('#menu-ul').html('');

          Object.keys(menu.menu).forEach(function(key) {
              if(key === 'table'){
                  var menuHTML = $('#menu-ul').html() + '<li id="toggle-table" class="table-toggler league-table">';
                  $('#menu-ul').html(menuHTML);
                  $('.league-table').html(table);
              } else {
                  var menuItem = menu.menu[key];
                  var menuHTML = $('#menu-ul').html() + '<li><a href="' + menuItem.href +'">' + (menuItem.text).toUpperCase() + '</a></li>';
                  $('#menu-ul').html(menuHTML);
              }
          });

          // Read the Allsvenska-table and populate the league-table
          edges[0].node.groups[0].standings.forEach(function(team){
              var pos = team.position;
              var name = team.team.name;
              var gamesPlayed = team.stats[0].value;
              var diff = team.stats[6].value;
              var points = team.stats[7].value;

              var table_entry = "<tr><td>"+pos+"</td><td>"+name+"</td><td>"+gamesPlayed+"</td><td>"+diff+"</td><td>"+points+"</td></tr>";
              var table_html =  $('.table-body').html() + table_entry;
              $('.table-body').html(table_html)
          });

          // Toggler for dropdown in responsive-mode
          $('#toggle-table').click(function(){
              $('#table').toggleClass('table-expand');
          });
          $('.menu-toggle').click(function() {
              $('#menu-bar-id').toggleClass('expand');
          });
    }

  render() {
      const {
        allEverysport: { edges },

      } = this.props.data;

    return (
        <div className="top-bar">
            <div className="menu-item logo"><img src={superettan} alt=""/></div>
            <div className="menu-item menu-toggle">&#9776;</div>
            <div id="menu-bar-id" class="menu-item menu-bar">            
            <ul id="menu-ul"><li><a href="#">LADDAR MENY.....</a></li><Table /></ul>
            </div>
            <div className="menu-item sponsors">
            <img class="select" src={select} alt=""/>
            <img class="opg" src={opg} alt=""/>
            </div>
            <div className="fade">
            </div>
            <div className="header-flow">
            </div>
        </div>
    )
  }
}

export default ClientFetchingExample
