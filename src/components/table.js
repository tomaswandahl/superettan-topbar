import React from "react"

/* eslint-disable */


export default props =><li id="toggle-table" className="table-toggler hidden league-table">
    <div className="toggler">
        <a href="#">TABELL</a>
        <svg id="arrow-drop-down-fill_1_" data-name="arrow-drop-down-fill (1)" xmlns="http://www.w3.org/2000/svg" width="25.993" height="25.993" viewBox="0 0 25.993 25.993">
            <path id="Path_14" data-name="Path 14" d="M0,0H25.993V25.993H0Z" fill="none"/>
            <path id="Path_15" data-name="Path 15" d="M12.332,14.332,8,10h8.664Z" transform="translate(0.664 0.83)" fill="#fff"/>
        </svg>
    </div>

    <table id="table" className="table">
    <thead>
    </thead>
    <tbody class="table-body">
        <tr className="header">
            <th colSpan="2"></th>
            <th>M</th>
            <th>+/-</th>
            <th>P</th>
        </tr>
    </tbody>
    </table>
</li>
