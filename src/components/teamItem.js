import React from "react"
import sef from "../img/sef.png"


export default props => <li><a href="#"><div className="team-img"><img src={sef} alt=""/></div><div className="team-name">{props.teamText}</div></a></li>
